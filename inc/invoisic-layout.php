<?php 
/**
 * WooInstant Layout Design
 *
 * @package  WooInstant
 */

defined( 'ABSPATH' ) || exit;

function invoisic_layout_shortcode_function( $atts ){
	?>
	<div class="page invoice-detail invoice-detail-draft invoice-loaded invoice-edit">
	   <div class="container">
	        <div class="row">
	            <div class="col-lg-9">

					<div class="invoice-actions d-print-none">
					    <div class="invoisic-btn-group btn-group-edit" role="group" aria-label="View Edit Invoice">
					        <button type="button" class="invoisic-btn btn-view ">Preview</button>
					        <button type="button" class="invoisic-btn btn-edit active" disabled="">Edit</button>
					    </div>
					    <div class="loading-indicator"></div>
					    <div class="invoice-actions-export">
					        <button disabled="" title="Record a payment from your client" class="invoisic-btn btn-payment">Record Payment</button>
					        <button title="Email the invoice to your client" class="invoisic-btn btn-email btn-prime">Email Invoice</button>
					    </div>
					</div>

					<div class="invoice-container">
					    <div class="invoice-detail-accent" style="background-color: rgb(51, 51, 51);"></div>
					    <div class="invoice-detail-body">
					        <div class="invoice-detail-title content-block">
					            <div class="d-flex justify-content-between">
					                <div class="invoice-title">
					                    <h2><input type="text" id="invoice-title" name="invoice-title" placeholder="Invoice" value=""></h2>
					                </div>
					                <div class="invoice-logo">
					                    
					                </div>
					            </div>
					        </div>

					        <div class="invoice-detail-header content-block">
					            <div class="row">
					                <div class="col-md invoice-address invoice-address-company">
					                    <h3>From</h3>
					                    <form>
					                        <div class="input-with-label-wrapper">
					                            <div class="label">
					                                <label for="invoice-company-name">Name</label>
					                            </div>
					                            <div class="content">
					                                <input type="text" id="invoice-company-name" name="invoice-company-name" autocomplete="organization" placeholder="Business Name" value="">
					                            </div>
					                        </div>
					                        <div class="input-with-label-wrapper">
					                            <div class="label">
					                                <label for="invoice-company-email">Email</label>
					                            </div>
					                            <div class="content">
					                                <div class="text-with-icon">
					                                    <input type="email" id="invoice-company-email" name="invoice-company-email" class="invoice-company-email" autocomplete="email" placeholder="name@business.com" value="">
					                                </div>
					                            </div>
					                        </div>
					                        <div class="input-with-label-wrapper">
					                            <div class="label with-baseline">
					                                <label for="invoice-company-address1">Address</label>
					                            </div>
					                            <div class="content">
					                                <div>
					                                    <input type="text" id="invoice-company-address1" name="invoice-company-address1" placeholder="Street" autocomplete="street-address" value="">
					                                </div>
					                            </div>
					                        </div>
					                        <div class="input-with-label-wrapper">
					                            <div class="label">
					                                <label for="invoice-company-phone">Phone</label>
					                            </div>
					                            <div class="content">
					                                <input type="text" id="invoice-company-phone" name="invoice-company-phone" data-selector="invoice-company-phone-input" placeholder="(123) 456 789" autocomplete="tel" value="">
					                            </div>
					                        </div>
					                        <div class="input-with-label-wrapper">
					                            <div class="label with-baseline">
					                                <label for="invoice-company-business-number" title="Business Number">Business Number</label>
					                            </div>
					                            <div class="content">
					                                <input type="text" id="invoice-company-business-number" name="invoice-company-business-number" placeholder="e.g. 123-45-6789" value="">
					                            </div>
					                        </div>
					                    </form>
					                </div>
					                <div class="col-md invoice-address invoice-address-client">
					                    <h3>To</h3>
					                    <div class="input-with-label-wrapper">
					                        <div class="label">
					                            <label for="invoice-client-name">Name</label>
					                        </div>
					                        <div class="content">
					                            <div class="client-suggest">
					                                <div role="combobox" aria-haspopup="listbox" aria-owns="react-autowhatever-1" aria-expanded="false" class="react-autosuggest__container">
					                                    <input type="text" autocomplete="off" aria-autocomplete="list" aria-controls="react-autowhatever-1" class="react-autosuggest__input" id="invoice-client-name" placeholder="Client Name" value="">
					                                    <div id="react-autowhatever-1" role="listbox" class="react-autosuggest__suggestions-container"></div>
					                                </div>
					                            </div>
					                        </div>
					                    </div>
					                    <div class="input-with-label-wrapper">
					                        <div class="label">
					                            <label for="invoice-client-email">Email</label>
					                        </div>
					                        <div class="content">
					                            <div class="text-with-icon">
					                                <input type="email" id="invoice-client-email" name="invoice-client-email" class="invoice-client-email" autocomplete="off" placeholder="name@client.com" value="">
					                            </div>
					                        </div>
					                    </div>
					                    <div class="input-with-label-wrapper">
					                        <div class="label with-baseline">
					                            <label for="invoice-client-address1">Address</label>
					                        </div>
					                        <div class="content">
					                            <div>
					                                <input type="text" id="invoice-client-address1" name="invoice-client-address1" placeholder="Street" autocomplete="" value="">
					                            </div>
					                        </div>
					                    </div>
					                    <div class="input-with-label-wrapper">
					                        <div class="label">
					                            <label for="invoice-client-phone">Phone</label>
					                        </div>
					                        <div class="content">
					                            <input type="text" id="invoice-client-phone" name="invoice-client-phone" data-selector="invoice-client-phone-input" placeholder="(123) 456 789" autocomplete="" value="">
					                        </div>
					                    </div>
					                </div>
					            </div>
					        </div>
					        <hr class="divider lite margin">
					        <div class="invoice-detail-terms content-block">
					            <div class="row">
					                <div class="col-md-6 invoice-address invoice-address-terms">
					                    <div class="input-with-label-wrapper">
					                        <div class="label with-baseline">
					                            <label for="invoice-number" class="invoice-number-label">Number</label>
					                        </div>
					                        <div class="content">
					                            <div class="text-with-icon input-with-error">
					                                <svg aria-hidden="true" focusable="false" data-prefix="fas" data-icon="check-circle" class="svg-inline--fa fa-check-circle fa-w-16 " role="img" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 512 512" color="grey">
					                                    <path fill="currentColor" d="M504 256c0 136.967-111.033 248-248 248S8 392.967 8 256 119.033 8 256 8s248 111.033 248 248zM227.314 387.314l184-184c6.248-6.248 6.248-16.379 0-22.627l-22.627-22.627c-6.248-6.249-16.379-6.249-22.628 0L216 308.118l-70.059-70.059c-6.248-6.248-16.379-6.248-22.628 0l-22.627 22.627c-6.248 6.248-6.248 16.379 0 22.627l104 104c6.249 6.249 16.379 6.249 22.628.001z"></path>
					                                </svg>
					                                <input type="text" id="invoice-number" class="invoice-number" maxlength="16" placeholder="INV0000" value="INV0001">
					                            </div>
					                        </div>
					                    </div>
					                    <div class="input-with-label-wrapper">
					                        <div class="label"><span>Date</span></div>
					                        <div class="content">
					                            <div class="react-datepicker-wrapper">
					                                <div class="react-datepicker__input-container">
					                                    <input type="text" id="invoice-date" name="invoice-date" class="invoice-detail-date" value="Aug 6, 2019">
					                                </div>
					                            </div>
					                        </div>
					                    </div>
					                    <div class="input-with-label-wrapper">
					                        <div class="label"><span>Terms</span></div>
					                        <div class="content">
					                            <div class="Select has-value is-searchable Select--single">
					                                <input name="invoice-terms-input" type="hidden" value="0">
					                                <div class="Select-control">
					                                    <div class="Select-multi-value-wrapper" id="react-select-2--value">
					                                        <div class="Select-value"><span class="Select-value-label" role="option" aria-selected="true" id="react-select-2--value-item">Due On Receipt</span></div>
					                                        <div class="Select-input" style="display: inline-block;">
					                                            <input id="invoice-terms-input" aria-activedescendant="react-select-2--value" aria-expanded="false" aria-haspopup="false" aria-owns="" role="combobox" value="" style="box-sizing: content-box; width: 5px;">
					                                            <div style="position: absolute; top: 0px; left: 0px; visibility: hidden; height: 0px; overflow: scroll; white-space: pre; font-size: 13px; font-family: &quot;Open Sans&quot;, Helvetica, Arial, sans-serif; font-weight: 400; font-style: normal; letter-spacing: normal; text-transform: none;"></div>
					                                        </div>
					                                    </div><span class="Select-arrow-zone"><span class="Select-arrow"></span></span>
					                                </div>
					                            </div>
					                        </div>
					                    </div>
					                </div>
					            </div>
					        </div>
					        <div class="invoice-item-list content-block">
					            <table class="table invoice-table invoice-table-edit">
					                <thead class="thead">
					                    <tr>
					                        <th class="invoice-detail-margin" style="background-color: rgb(51, 51, 51) !important; color: rgb(255, 255, 255);">&nbsp;</th>
					                        <th class="invoice-detail-summary" style="background-color: rgb(51, 51, 51) !important; color: rgb(255, 255, 255);">Description</th>
					                        <th class="invoice-detail-rate" style="background-color: rgb(51, 51, 51) !important; color: rgb(255, 255, 255);">Rate</th>
					                        <th class="invoice-detail-quantity" style="background-color: rgb(51, 51, 51) !important; color: rgb(255, 255, 255);">Qty</th>
					                        <th class="invoice-detail-total" style="background-color: rgb(51, 51, 51) !important; color: rgb(255, 255, 255);">Amount</th>
					                        <th class="invoice-detail-tax" style="background-color: rgb(51, 51, 51) !important; color: rgb(255, 255, 255);">Tax</th>
					                    </tr>
					                </thead>
					                <tbody class="invoice-items">
					                    <tr class="item-row item-row-1 with-tax">
					                        <td class="item-row-actions">
					                            <div class="confirm-delete-button">
					                                <button title="Remove Item" class="btn btn-remove" style="border-color: rgb(51, 51, 51); color: rgb(51, 51, 51);">
					                                    <svg aria-hidden="true" focusable="false" data-prefix="fas" data-icon="times" class="svg-inline--fa fa-times fa-w-11 " role="img" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 352 512">
					                                        <path fill="currentColor" d="M242.72 256l100.07-100.07c12.28-12.28 12.28-32.19 0-44.48l-22.24-22.24c-12.28-12.28-32.19-12.28-44.48 0L176 189.28 75.93 89.21c-12.28-12.28-32.19-12.28-44.48 0L9.21 111.45c-12.28 12.28-12.28 32.19 0 44.48L109.28 256 9.21 356.07c-12.28 12.28-12.28 32.19 0 44.48l22.24 22.24c12.28 12.28 32.2 12.28 44.48 0L176 322.72l100.07 100.07c12.28 12.28 32.2 12.28 44.48 0l22.24-22.24c12.28-12.28 12.28-32.19 0-44.48L242.72 256z"></path>
					                                    </svg>
					                                </button>
					                            </div>
					                        </td>
					                        <td data-label="Item #1" class="item-row-summary"><span class="item-row-name"><div class="item-suggest"><div role="combobox" aria-haspopup="listbox" aria-owns="react-autowhatever-1" aria-expanded="false" class="react-autosuggest__container"><input type="text" autocomplete="off" aria-autocomplete="list" aria-controls="react-autowhatever-1" class="react-autosuggest__input" id="invoice-item-code" placeholder="Item Description" value=""><div id="react-autowhatever-1" role="listbox" class="react-autosuggest__suggestions-container"></div></div></div></span><span class="item-row-description"><textarea class="item-description-input" placeholder="Additional details" style="min-height: 80px; height: 87px;"></textarea></span></td>
					                        <td data-label="Price" class="item-row-rate"><span class="react-numeric-input"><input type="text" placeholder="0.00" value="" pattern=".*"><b><i></i></b><b><i></i></b></span></td>
					                        <td data-label="Quantity" class="item-row-quantity"><span class="react-numeric-input"><input type="text" placeholder="0" value="1" pattern=".*"><b><i></i></b><b><i></i></b></span></td>
					                        <td data-label="Amount" class="item-row-amount"><span class="currency edit amount"><span class="localized-number">BDT&nbsp;0.00</span></span>
					                        </td>
					                        <td class="item-row-tax">
					                            <button class="btn-checkbox invoice-item-tax-enabled" style="color: rgb(51, 51, 51);">
					                                <svg aria-hidden="true" focusable="false" data-prefix="fas" data-icon="check-square" class="svg-inline--fa fa-check-square fa-w-14 " role="img" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 448 512">
					                                    <path fill="currentColor" d="M400 480H48c-26.51 0-48-21.49-48-48V80c0-26.51 21.49-48 48-48h352c26.51 0 48 21.49 48 48v352c0 26.51-21.49 48-48 48zm-204.686-98.059l184-184c6.248-6.248 6.248-16.379 0-22.627l-22.627-22.627c-6.248-6.248-16.379-6.249-22.628 0L184 302.745l-70.059-70.059c-6.248-6.248-16.379-6.248-22.628 0l-22.627 22.627c-6.248 6.248-6.248 16.379 0 22.627l104 104c6.249 6.25 16.379 6.25 22.628.001z"></path>
					                                </svg>
					                            </button>
					                        </td>
					                    </tr>
					                    <tr class="item-row item-row-n" style="border-top: 1px solid rgb(51, 51, 51);">
					                        <td class="item-row-actions" colspan="6">
					                            <button id="invoice-item-add" title="Add Item" class="btn btn-prime btn-add" style="background: rgb(51, 51, 51); border-color: rgb(51, 51, 51);">
					                                <svg aria-hidden="true" focusable="false" data-prefix="fas" data-icon="plus" class="svg-inline--fa fa-plus fa-w-14 " role="img" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 448 512">
					                                    <path fill="currentColor" d="M416 208H272V64c0-17.67-14.33-32-32-32h-32c-17.67 0-32 14.33-32 32v144H32c-17.67 0-32 14.33-32 32v32c0 17.67 14.33 32 32 32h144v144c0 17.67 14.33 32 32 32h32c17.67 0 32-14.33 32-32V304h144c17.67 0 32-14.33 32-32v-32c0-17.67-14.33-32-32-32z"></path>
					                                </svg>
					                            </button>
					                        </td>
					                    </tr>
					                </tbody>
					            </table>
					        </div>
					        <hr class="divider margin" style="background-color: rgb(51, 51, 51);">
					        <div class="invoice-totals-container content-block">
					            <div class="invoice-totals-row invoice-summary-subtotal desktop">
					                <div class="invoice-summary-label">Subtotal</div>
					                <div class="invoice-summary-value"><span class="currency"><span class="localized-number">BDT&nbsp;0.00</span></span>
					                </div>
					            </div>
					            <div class="invoice-totals-row desktop">
					                <div class="invoice-summary-label">Tax (0%)</div>
					                <div class="invoice-summary-value"><span class="currency"><span class="localized-number">BDT&nbsp;0.00</span></span>
					                </div>
					            </div>
					            <div class="invoice-totals-row desktop" data-selector="invoice-total">
					                <div class="invoice-summary-label">Total</div>
					                <div class="invoice-summary-value"><span class="currency"><span class="localized-number">BDT&nbsp;0.00</span></span>
					                </div>
					            </div>
					            <div class="invoice-totals-row invoice-summary-balance invoice-balance bold desktop" data-selector="invoice-balance">
					                <div class="invoice-summary-label">Balance Due</div>
					                <div class="invoice-summary-value"><span class="currency"><span class="localized-number">BDT&nbsp;0.00</span></span>
					                </div>
					            </div>
					        </div>
					        <hr class="divider lite margin">
					        <div class="invoice-detail-footer content-block">
					            <div class="invoice-detail-notes clearfix">
					                <textarea id="invoice-notes" class="invoice-notes-input" placeholder="Notes - any relevant information not covered, additional terms and conditions" style="height: 113px;"></textarea>
					            </div>
					        </div>
					    </div>
					</div>

	            </div>
	            <div class="col-lg-3"></div>
	        </div>
	    </div>	
	</div>
	<?php
}
add_shortcode( 'invoisic', 'invoisic_layout_shortcode_function' );
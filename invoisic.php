<?php
/**
 * Plugin Name: Invoisic - Frontend Estimates and Invoices for WordPress
 * Plugin URI: https://bitbucket.org/devshuvo/invoisic
 * Bitbucket Plugin URI: https://bitbucket.org/devshuvo/invoisic
 * Description: A FLEXIBLE, WELL-SUPPORTED, AND EASY-TO-USE WORDPRESS INVOICING PLUGIN TO CREATE PROFESSIONAL QUOTES AND INVOICES THAT CLIENTS CAN PAY FOR ONLINE.
 * Author: Themevanilla
 * Text Domain: invoisic
 * Domain Path: /lang/
 * Author URI: https://psdtowpservice.com
 * Tags: invoisic,responsive,woocommerce
 * Version: 1.0.0
 */

// don't load directly
if ( ! defined( 'ABSPATH' ) ) {
	die( '-1' );
}

/**
* Including Plugin file for security
* Include_once
* 
* @since 1.0.0
*/
include_once( ABSPATH . 'wp-admin/includes/plugin.php' );

/**
* Loading Text Domain
* 
*/
add_action('plugins_loaded', 'invoisic_plugin_loaded_action', 10, 2);

function invoisic_plugin_loaded_action() {
	//Internationalization 
	load_plugin_textdomain( 'wooinstant', false, dirname( plugin_basename(__FILE__) ) . '/lang/' );
	
	//Redux Framework calling
	if ( !class_exists( 'ReduxFramework' ) && file_exists( dirname( __FILE__ ) . '/inc/redux-framework/ReduxCore/framework.php' ) ) {
	    require_once( dirname( __FILE__ ) . '/inc/redux-framework/ReduxCore/framework.php' );
	}

    // Load the plugin options
    if ( !isset( $redux_demo ) && file_exists( dirname( __FILE__ ) . '/inc/invoisic-options-init.php' ) ) {
        require_once dirname( __FILE__ ) . '/inc/invoisic-options-init.php';
    }
	
}

/**
 *	Enqueue Wooinstant scripts
 *
 */
function invoisic_enqueue_scripts(){	

	wp_enqueue_style('font-awesome', plugin_dir_url( __FILE__ ) . 'assets/css/font-awesome.min.css' );
	wp_enqueue_style('bootstrap-grids', plugin_dir_url( __FILE__ ) . 'assets/css/bootstrap-grids.css' );
	wp_enqueue_style('invoisic-stylesheet', plugin_dir_url( __FILE__ ) . 'assets/css/styles.css' );



    wp_enqueue_script( 'invoisic-script', plugin_dir_url( __FILE__ ) . 'assets/js/wi-ajax-script.js', array('jquery'), '2.0.05', true );
	
	
	
	wp_localize_script( 'invoisic-script', 'invoisic_params', 
		array(
	        'ajax_nonce' => wp_create_nonce( 'invoisic_ajax_nonce' ),
	        'ajax_url' => admin_url( 'admin-ajax.php' )
	    )
    );

}
add_filter( 'wp_enqueue_scripts', 'invoisic_enqueue_scripts', 200 ,2 ); // Giving high priority 

/**
 *	Invoisic page layout
 */
require_once( dirname( __FILE__ ) . '/inc/invoisic-layout.php' );

/**
 *	Invoisic Functions
 */
require_once( dirname( __FILE__ ) . '/inc/invoisic-functions.php' );

/**
 *	Plugin activation hook
 *
 */
function wooinstant_activation_redirect( $plugin ) {
	if( $plugin == plugin_basename( __FILE__ ) ) {
	    // redirect option page after installed
	    wp_redirect( admin_url( 'admin.php?page=_woinstant' ) );
	    exit;
	}
}
//add_action( 'activated_plugin', 'wooinstant_activation_redirect' );

/**
 *	Plugin Deactivate Confirmation
 *
 */
if( !function_exists('wooinstant_plugin_admin_footer_scripts') ){
	function wooinstant_plugin_admin_footer_scripts(){
		ob_start(); ?>		
		<script type="text/javascript">
			jQuery( function($) {
				jQuery(document).ready( function(){
					//plugin deactive
				    jQuery(document).on('click', '[data-plugin="wooinstant/wooinstant.php"] .deactivate>a', function(e) {
				        e.preventDefault()
				        var urlRedirect = jQuery(this).attr('href');
				        if ( confirm( 'Are you sure ?\n( You will lose your saved options )' ) ) {
				            window.location.href = urlRedirect;
				        } else {
				            //what else ?
				        }
				    });

				    //Notice Dismiss
				    $('.wi-notice a,.wi-notice button').on('click',function(e){
				    	//e.preventDefault();
				    	//var data = $(this).data('value');

			            $.post(ajaxurl, {
			                action: "dismissnotice",
			                dismiss: 1,
			            }, function (data) {
			            });

			            $('.wi-notice').fadeOut();
				    })

				});
			});
		</script> <?php
		echo ob_get_clean();
	}
}
//add_action( 'admin_footer', 'wooinstant_plugin_admin_footer_scripts' );

/**
 * Notice if WooCommerce is inactive
 */
function wooinstant_admin_notice_warn() {
	if ( !class_exists( 'WooCommerce' ) ) { ?>
	    <div class="notice notice-warning is-dismissible">
	        <p>
	        	<strong><?php esc_attr_e( 'Wooinstant requires WooCommerce to be activated ', 'wooinstant' ); ?> <a href="<?php echo esc_url( admin_url('/plugin-install.php?s=WooCommerce&tab=search&type=term') ); ?>">Install Now</a></strong>
	        </p>
	    </div> <?php
    }
    if( get_option( "dismiss-notice") != 1 ){ ?>
		<div class="wi-notice notice notice-info is-dismissible">		
			<style>
				.wi-notice{
					clear: both;
					overflow: hidden;
				}
				.wi-notice .dashicons{
					height: unset;
					width: unset;
					font-size: 12px;
					vertical-align: middle;
				}
				.wi-notice a{
					margin-left: 10px;
					text-decoration: none;
					cursor: pointer;
				}
			</style>
		    <p style="float: left;">If you like <strong>Wooinstant</strong> please leave a review</p>
		    <p style="float: right;">
		    	<a data-value="1" href="https://codecanyon.net/downloads" target="_blank">Rate Us <span class="dashicons dashicons-star-filled"></span><span class="dashicons dashicons-star-filled"></span><span class="dashicons dashicons-star-filled"></span><span class="dashicons dashicons-star-filled"></span><span class="dashicons dashicons-star-filled"></span></a>
		    	<a data-value="1" target="">Maybe later</a>
		    	<a data-value="1" target="">Already Rated <span class="dashicons dashicons-smiley"></span></a>
		    </p>
		</div> <?php 
	}
}
//add_action( 'admin_notices', 'wooinstant_admin_notice_warn' );

function wi_notice_ajax_function(){
	if ( isset( $_POST['dismiss'] ) && $_POST['dismiss'] == 1 ) {
		update_option( "dismiss-notice", 1 );
	}
	wp_die( 'done' );
}
//add_action('wp_ajax_dismissnotice','wi_notice_ajax_function');

/**
 * Adds plugin action links.
 *
 * @since 1.0.0
 * @version 4.0.0
 */
function wi_plugin_action_links( $links ) {
	$plugin_links = array(
		'<a href="admin.php?page=_woinstant">' . esc_html__( 'Settings', 'wooinstant' ) . '</a>',
	);
	return array_merge( $plugin_links, $links );
}
//add_filter( 'plugin_action_links_' . plugin_basename( __FILE__ ), 'wi_plugin_action_links' );